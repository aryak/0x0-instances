#!/usr/bin/env python

import requests
import json

def main():
    ## Make the request
    #request = requests.get("https://codeberg.org/aryak/0x0-instances/raw/branch/main/README.md")
    ## Check if it worked
    #if request.status_code != 200:
    #    print("Uh oh, could not connect to source...")
    #data = request.text

    # Get that README
    with open("README.md", "r") as f:
        data = f.read()
    # Make it a list of lines
    datasplit = data.splitlines()
    # Find out where is the table and get the indexes
    tablelines = []
    for line in datasplit:
        if line.startswith("|"):
            tablelines.append(datasplit.index(line))
    # Find the categories: Instance, Country, Provider... etc
    categories = datasplit[tablelines[0]][2:][:-2].split(" | ")

    # Making that json :)
    finale = []
    # Looping through lines of the table
    for instance in datasplit[tablelines[2]:tablelines[-1]]:
        # Cleaning up the markdown poop
        instance = instance[2:][:-2].split(" | ")
        # Cleaning up more markdown poop
        if "[" in instance[0]:
            instance[0] = instance[0].split("(")[1].replace(")", "")
        if "[" in instance[6]:
            instance[6] = instance[6].split("(")[1].replace(")", "")
        # These are the items with the data
        item = {
            categories[0]: instance[0],
            categories[1]: instance[1],
            categories[2]: instance[2],
            categories[3]: instance[3],
            categories[4]: instance[4],
            categories[5]: instance[5],
            categories[6]: instance[6]
        }
        # COMBINE!!!!!
        finale.append(item)
    # Dump that into a json file
    with open("0x0-instances.json", "w") as f:
        f.write(json.dumps(finale, indent=4))
    print("Converted README.md to json file.")

if __name__ == "__main__":
    main()
