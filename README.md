# 0x0 instance list

Unofficial instance list for 0x0, the null-pointer

| Instance | Country | Provider | File Storage | Max File-Size | Retention | Operator |
|----------|---------|----------|--------------|---------------|-----------|----------|
| https://0x0.st | DE | Hetzner | Hetzner | 512 MiB | 197.5 days for 128 MiB | 0x0 developer |
| https://envs.sh | DE | myloc.de | myloc.de | 512 MiB | 197.5 days for 128 MiB | [envs.net](https://envs.net) |
| https://null.slipfox.xyz | US | Hetzner | Hetzner | 256 MiB | 197.5 days for 128MiB | [Slipfox Network Suite](https://slipfox.xyz) |
| https://ttm.sh | CA | OVH | OVH | 256 MiB | 197.5 days for 64 MiB | [~team](https://tilde.team) |
| https://0.vern.cc | US | Hetzner | Local Server | 1024 MiB | Forever | [~vern](https://vern.cc) |
| [0.vernccvbvyi5...onion](http://0.vernccvbvyi5qhfzyqengccj7lkove6bjot2xhh5kajhwvidqafczrad.onion) | N/A | Hetzner | Local Server | 1024 MiB | Forever | [~vern](https://vernccvbvyi5qhfzyqengccj7lkove6bjot2xhh5kajhwvidqafczrad.onion) |
| [http://0.vern.i2p](http://vernkjqjz3qctifc3ovi7s77zzkej6qb6wbgly7yu46tgtffskla.b32.i2p) | N/A | Hetzner | Local Server | 1024 MiB | Forever | [~vern](http://verncceu2kgz54wi7r5jatgmx2mqtsh3knxhiy4m5shescuqtqfa.b32.i2p) |

Note: All instances are arranged alphabetically to lessen bias.

## How to add your instance?
Just submit a PR or email me a patch at arya@projectsegfau.lt